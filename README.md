Every Template is a Component
=============================

The theme is a migration of [the core starterkit theme](https://git.drupalcode.org/project/drupal/-/tree/11.x/core/themes/starterkit_theme) to use [SDC](https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/about-single-directory-components) components. There is one-to-one relation between a regular Drupal template file and a component which is included in the template instead of markup.

No more libraries in a `*.info.yml` or `*.libraries.yml` files. The same with css, js, or other asset folders in your theme's root directory. Everything what is required by a component or possibly its children resides in that component directory.

Out of the box the theme provides integration with the [Storybook](https://storybook.js.org/docs/html/get-started/whats-a-story) so you can see each of the components (templates) isolated in an iframe with all the asset files required to display that component. Play with the Storybook controls or edit any file of a component and review changes immediately in the iframe.

in essence, the Storybook works as a decoupled site for Drupal entities, fields, views, blocks, forms or form elements and even pages. Thanks to the great [Component Libraries: Theme Server](https://www.drupal.org/project/cl_server) and [Twig Tweak](https://www.drupal.org/project/twig_tweak/) modules the Storybook stories are easy to create or modify.


Installation
------------

It is recommended to install the theme on a fresh Drupal install though on a full blown development site it should also work.

Example CLI commands need to be run from your Drupal project root. If you use tools like [ddev](https://ddev.readthedocs.io) then login into its shell first: `ddev ssh`

1. Allow composer dev packages and download this theme with dependencies:
```console
composer config minimum-stability dev && composer require drupal/etc
```
  
2. Install dependencies first:
```console
drush pm:install sdc media media_library cl_server twig_tweak  
```

3. Install the theme and set it default along with admin theme. It will also create *My Frontpage* view and *My Article* content type with a set of fields to theme on:
```console
drush theme:install etc && drush config-set system.theme default etc && drush config-set system.theme admin etc
```

4. Optionally download other useful modules:
```console
composer require drupal/devel drupal/cl_devel drupal/cl_generator drupal/remove_http_headers
```

5. Enable additional modules:
```console
drush pm:install devel devel_generate cl_devel cl_generator remove_http_headers
```

Development environment set up
------------------------------

1. The `remove_http_headers` module helps to display *Remote Video* media type in the Storybook. Set to remove the `X-Frame-Options` header from the response:
```console
drush config-set --input-format=yaml remove_http_headers.settings headers_to_remove [X-Frame-Options]
```

2. Copy the `example.settings.local.php` to `settings.local.php`:
```console
cp web/sites/example.settings.local.php web/sites/default/settings.local.php
```

3. Make sure this file is included in the `web/sites/default/settings.php`:
```console
echo -e 'if (file_exists($app_root . "/" . $site_path . "/settings.local.php")) {\n\tinclude $app_root . "/" . $site_path . "/settings.local.php";\n}' >> web/sites/default/settings.php
```

4. Add the following lines to the file:
```console
echo -e '$settings["container_yamls"][] = DRUPAL_ROOT . "/sites/my_dev.services.yml";\n$settings["cache"]["bins"]["bootstrap"] = "cache.backend.null";\n$settings["cache"]["bins"]["discovery"] = "cache.backend.null";\n$settings["config_exclude_modules"] = ["devel","cl_devel","remove_http_headers"];\n$settings["twig_tweak_enable_php_filter"] = TRUE;' >> web/sites/default/settings.local.php
```

5. Add twig and cors settings into `my_dev.services.yml` file:
```console
echo -e 'parameters:\n\thttp.response.debug_cacheability_headers: true\n\ttwig.config:\n\t\tdebug: true\n\t\tcache: false\n\tcors.config:\n\t\tenabled: true\n\t\tallowedHeaders: ["*"]\n\t\tallowedMethods: []\n\t\tallowedOrigins: ["*"]\n\t\texposedHeaders: false\n\t\tmaxAge: false\n\t\tsupportsCredentials: true\nservices:\n\tcache.backend.null:\n\t\tclass: Drupal\Core\Cache\NullBackendFactory' > web/sites/my_dev.services.yml && sed -i 's/\t/  /g' web/sites/my_dev.services.yml
```

6. Add required permissions to anonymous user:
```console
drush ev "\Drupal\user\Entity\Role::load('anonymous')->grantPermission('use cl server')->grantPermission('access user profiles')->save();"
```
7. Check the issue below and if that is not fixed apply the issue patch to the `cl_server` module.

https://www.drupal.org/project/cl_server/issues/3381368
```console
cd web/modules/contrib/cl_server && wget https://git.drupalcode.org/project/cl_server/-/merge_requests/17.diff && patch -p1 < 17.diff && cd -
```

8. Do not forget to rebuild caches:
```console
drush cache:rebuild
```

Generate content for development
----------------------------------

The Storybook stories by default have controls referenced either to `my_article` content type or to the node of this type with ID == 1. You can play with these values directly in the Storybook or edit story files for your needs.

1. **Manually** create `media` entities, one per each of the available media types.

2. Devel Generate `term` entities.

3. Devel Generate *My Article* `content` entities. Make sure the *Maximum number of comments per node* field has some value on the generation form.

Storybook set up
----------------

The Storybook stories are built around regular twig inline template with extensive usage of the *Twig Tweak* functions and filters, particularly the **PHP** filter. That means any code you may execute in your Drupal `*.php` file also could be remotely executed in a `content` control of the Storybook component.

**Never run Storybook on a production Drupal site!**

Also, when sharing Storybook on your LAN make sure only trusted users have access to the running app.

1. If you want to install Storybook in this theme's folder please apply the following patch:

https://www.drupal.org/project/cl_server/issues/3369045 
  
2. Otherwise, copy Storybook assets to your project root. Make sure to not overwrite existing files with the same name in the root:
```console  
cp -r web/themes/contrib/etc/.storybook/ . && cp web/themes/contrib/etc/package.json . && cp web/themes/contrib/etc/package-lock.json .
```

3. Edit files in the `.storybook` folder for your needs.

4. Install npm packages:
```console
npm install
```

5. Run the Storybook app. The IP version of the Storybook URl might be shared on your LAN:
```console
npm run storybook
```

If the above does not work refer to the [cl_server module docs](https://git.drupalcode.org/project/cl_server/-/blob/2.x/docs/storybook.md).

Renaming the theme
------------------

1. If you want to rename all the files and content to your own machine name, like - `my_theme`; run the following:
```console
php web/core/scripts/drupal generate-theme my_theme --starterkit etc
```

2. Enable your newly generated theme and uninstall the `etc` theme:
```console
drush theme:install my_theme && drush config-set system.theme default my_theme && drush config-set system.theme admin my_theme && drush theme:uninstall etc
```

3. Add dependencies of the new theme to `composer.json` file and remove `etc` theme from your project:
```console
composer require drupal/cl_server drupal/twig_tweak && composer remove drupal/etc
```

### HAPPY THEMING!

### Author

*Vlad Proshin*
https://drupal.org/u/drugan
