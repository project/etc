/** @type { import('@storybook/server').Preview } */
const preview = {
  globals: {
    drupalTheme: 'etc',
    supportedDrupalThemes: {
      etc: {title: 'etc'},
      olivero: {title: 'Olivero'},
      claro: {title: 'Claro'},
    },
  },
  parameters: {
    server: {
      // Replace this with your Drupal site URL, or an environment variable.
      url: 'http://drupal-site.com',
      // Use your local IP instead to share running storybook on LAN.
      // https://ddev.readthedocs.io/en/latest/users/topics/sharing
    },
    // https://github.com/Sirrine-Jonathan/storybook-source-link
    // sourceLinkPrefix: 'Put here "url" value from the above parameter.',
  },
};

export default preview;
