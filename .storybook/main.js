/** @type { import('@storybook/server-webpack5').StorybookConfig } */
const config = {
  // Change the place where storybook searched for stories.
  stories: [
//     Storybook installed in this theme folder:
//     "../components/**/*.stories.yml",
//     Storybook installed in the project root:
    "../web/themes/**/*.stories.yml",
//    "../web/themes/**/*.stories.@(json|yml)",
//    "../web/themes/**/*.stories.mdx",
//    "../web/modules/**/*.stories.mdx",
//    "../web/modules/**/*.stories.@(json|yml)",
  ],
  // ...
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    // https://github.com/lullabot/storybook-drupal-addon
    '@lullabot/storybook-drupal-addon', // <----
    // https://storybook.js.org/addons/@storybook/addon-a11y
    '@storybook/addon-a11y',
    // https://storybook.js.org/addons/@whitespace/storybook-addon-html
    '@whitespace/storybook-addon-html',
    // https://github.com/jorgenskogmo/storybook-addon-validate-html
    // 'storybook-addon-validate-html',
    // https://github.com/Sirrine-Jonathan/storybook-source-link
    // 'storybook-source-link',
    // https://storybook.js.org/addons/@storybook/addon-docs
    // 'addon-docs',
  ],
  docs: {
    autodocs: "tag",
  },
  framework: {
    name: "@storybook/server-webpack5",
    options: {},
  },
  core: {
    builder: '@storybook/builder-webpack5'
  }
};

export default config;
