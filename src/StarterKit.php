<?php

namespace Drupal\etc;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Theme\StarterKitInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Twig\Util\TemplateDirIterator;

/**
 * Makes additional cleanup.
 */
final class StarterKit implements StarterKitInterface {

  /**
   * {@inheritdoc}
   */
  public static function postProcess(string $tmp_dir, string $destination_theme, string $theme_name): void {
    $io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

    foreach (['src', 'config', '.git', 'node_modules'] as $dir) {
      if (is_dir($tmp_dir . DIRECTORY_SEPARATOR . $dir)) {
        static::removemDirRecursive($tmp_dir . DIRECTORY_SEPARATOR . $dir);
        if ($dir == '.git') {
          $io->warning("The '.git' directory of the starterkit was removed so you can 'git init' you own one.");
        }
        elseif ($dir == 'node_modules') {
          $io->warning("The 'node_modules' directory was removed, you need to reinstall packages by running 'npm install' command.");
        }
      }
    }
    unlink($tmp_dir . DIRECTORY_SEPARATOR . 'composer.json');

    $extensions = implode('|', [
      preg_quote('.twig'),
      preg_quote('.yml'),
      preg_quote('.css'),
      preg_quote('.js'),
      preg_quote('.md'),
    ]);

    $iterator = new TemplateDirIterator(new \RegexIterator(
      new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($tmp_dir), \RecursiveIteratorIterator::LEAVES_ONLY
      ), '/(' . $extensions . ')$/'
    ));

    $error = NULL;
    foreach ($iterator as $file => $contents) {
      $new_content = str_replace('etc', $destination_theme, $contents);
      if (str_ends_with($file, '.stories.yml')) {
        $new_content = str_replace("from 'themes/contrib/", "from 'themes/", $new_content);
      }
      if ($error = !file_put_contents($file, $new_content)) {
        $io->getErrorStyle()->error("The file $file could not be written.\nPlease fix this issue, remove generated theme and try again.");
        break;
      }
    }

    if (!$error) {
      $file = "$tmp_dir/$destination_theme.info.yml";
      $info = Yaml::decode(file_get_contents($file));
      unset($info['hidden'], $info['starterkit']);
      $info['generator'] = str_replace($destination_theme, 'etc', $info['generator']);
      file_put_contents($file, Yaml::encode($info));

      $io->warning("All instances of the 'etc' were replaced with the '$destination_theme' string.");
      $io->success('
██╗  ██╗ █████╗ ██████╗ ██████╗ ██╗   ██╗
██║  ██║██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
███████║███████║██████╔╝██████╔╝ ╚████╔╝
██╔══██║██╔══██║██╔═══╝ ██╔═══╝   ╚██╔╝
██║  ██║██║  ██║██║     ██║        ██║
╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝        ╚═╝

████████╗██╗  ██╗███████╗███╗   ███╗██╗███╗   ██╗ ██████╗ ██╗
╚══██╔══╝██║  ██║██╔════╝████╗ ████║██║████╗  ██║██╔════╝ ██║
   ██║   ███████║█████╗  ██╔████╔██║██║██╔██╗ ██║██║  ███╗██║
   ██║   ██╔══██║██╔══╝  ██║╚██╔╝██║██║██║╚██╗██║██║   ██║╚═╝
   ██║   ██║  ██║███████╗██║ ╚═╝ ██║██║██║ ╚████║╚██████╔╝██╗
   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝');
    }
  }

  /**
   * Removes directory recursively.
   *
   * @param string $dir
   *   The directory to be removed.
   */
  private static function removemDirRecursive(string $dir): void {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
            static::removemDirRecursive($dir . DIRECTORY_SEPARATOR . $object);
          }
          else {
            unlink($dir . DIRECTORY_SEPARATOR . $object);
          }
        }
      }
      rmdir($dir);
    }
  }

}
