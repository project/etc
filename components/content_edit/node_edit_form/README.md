# Node edit form

### Include

```html
{% include 'etc:node_edit_form' %}
```

```html
{% include 'etc:node_edit_form' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
```
### Embed

```html
{% embed 'etc:node_edit_form' %}
{% endembed %}
```

```html
{% embed 'etc:node_edit_form' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
  {% block content %}
    <div class="block-etc-node-edit-form">
      <p class="block-etc-node-edit-form__above">Example above message.</p>
      <div class="block-etc-node-edit-form__wrapper">{{ parent() }}</div>
      <p class="block-etc-node-edit-form__below">Example below message.</p>
    </div>
  {% endblock %}
{% endembed %}
```

### Render

```php
[
  '#type' => 'component',
  '#component' => 'etc:node_edit_form',
  '#props' => [
    'example_array_prop' => [
      ['example_one' => "#", 'example_two' => "First Example replacement item"],
      ['example_one' => "#", 'example_two' => "Second Example replacement item"],
    ],
  ],
//   '#slots' => [
//     'content' => [
//       '#type' => 'html_tag',
//       '#tag' => 'p',
//       '#value' => 'This is the replacement of the component contents.',
//     ],
//   ],
];
```
