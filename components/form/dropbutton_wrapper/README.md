# Dropbutton wrapper

### Include

```html
{% include 'etc:dropbutton_wrapper' %}
```

```html
{% include 'etc:dropbutton_wrapper' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
```
### Embed

```html
{% embed 'etc:dropbutton_wrapper' %}
{% endembed %}
```

```html
{% embed 'etc:dropbutton_wrapper' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
  {% block content %}
    <div class="block-etc-dropbutton-wrapper">
      <p class="block-etc-dropbutton-wrapper__above">Example above message.</p>
      <div class="block-etc-dropbutton-wrapper__wrapper">{{ parent() }}</div>
      <p class="block-etc-dropbutton-wrapper__below">Example below message.</p>
    </div>
  {% endblock %}
{% endembed %}
```

### Render

```php
[
  '#type' => 'component',
  '#component' => 'etc:dropbutton_wrapper',
  '#props' => [
    'example_array_prop' => [
      ['example_one' => "#", 'example_two' => "First Example replacement item"],
      ['example_one' => "#", 'example_two' => "Second Example replacement item"],
    ],
  ],
//   '#slots' => [
//     'content' => [
//       '#type' => 'html_tag',
//       '#tag' => 'p',
//       '#value' => 'This is the replacement of the component contents.',
//     ],
//   ],
];
```
