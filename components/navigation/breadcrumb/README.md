# Breadcrumb

Only **{% include|embed 'provider:component' %}** are required.

### Include
```html
{% include 'etc:breadcrumb' ignore missing with {breadcrumb: [
  {url: "#", text: "First Example replacement item"},
  {url: "#", text: "Second Example replacement item"}
]} only %}
```
### Embed

```html
{% embed 'etc:breadcrumb' ignore missing with {breadcrumb: [
  {url: "#", text: "First Example replacement item"},
  {url: "#", text: "Second Example replacement item"}
]} only %}
  {% block content %}
    <div class="block-etc-breadcrumb">
      <p class="block-etc-breadcrumb__above">Example above message.</p>
      <div class="block-etc-breadcrumb__wrapper">{{ parent() }}</div>
      <p class="block-etc-breadcrumb__below">Example below message.</p>
    </div>
  {% endblock %}
{% endembed %}
```

### Render

```php
[
  '#type' => 'component',
  '#component' => 'etc:breadcrumb',
  '#props' => [
    'breadcrumb' => [
      ['url' => "#", 'text' => "First Example replacement item"],
      ['url' => "#", 'text' => "Second Example replacement item"],
    ],
  ],
//   '#slots' => [
//     'content' => [
//       '#type' => 'html_tag',
//       '#tag' => 'p',
//       '#value' => 'This is the replacement of the component contents.',
//     ],
//   ],
];
```
