# Local tasks block

### Include

```html
{% include 'etc:local_tasks_block' %}
```

```html
{% include 'etc:local_tasks_block' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
```
### Embed

```html
{% embed 'etc:local_tasks_block' %}
{% endembed %}
```

```html
{% embed 'etc:local_tasks_block' ignore missing with {example_array_prop: [
  {example_one: "#", example_two: "First Example replacement item"},
  {example_one: "#", example_two: "Second Example replacement item"}
]} only %}
  {% block content %}
    <div class="block-etc-local-tasks-block">
      <p class="block-etc-local-tasks-block__above">Example above message.</p>
      <div class="block-etc-local-tasks-block__wrapper">{{ parent() }}</div>
      <p class="block-etc-local-tasks-block__below">Example below message.</p>
    </div>
  {% endblock %}
{% endembed %}
```

### Render

```php
[
  '#type' => 'component',
  '#component' => 'etc:local_tasks_block',
  '#props' => [
    'example_array_prop' => [
      ['example_one' => "#", 'example_two' => "First Example replacement item"],
      ['example_one' => "#", 'example_two' => "Second Example replacement item"],
    ],
  ],
//   '#slots' => [
//     'content' => [
//       '#type' => 'html_tag',
//       '#tag' => 'p',
//       '#value' => 'This is the replacement of the component contents.',
//     ],
//   ],
];
```
